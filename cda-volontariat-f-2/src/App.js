import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'mdbreact/dist/css/mdb.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from './Pages/Accueil';
import Menu from './Pages/Menu';
import AnnonceAnimaux from './Pages/AnnonceAnimaux';
import AnnonceCourses from './Pages/AnnonceCourses';
import AnnonceJardinage from './Pages/AnnonceJardinage';


function App() {
  return (
    <Router>
      <Switch>

      <Route exact path="/" component={Login}/>
      <Route exact path="/menu" component={Menu}/>
      <Route exact path="/animaux" component={AnnonceAnimaux}/>
      <Route exact path='/courses' component={AnnonceCourses}/>
      <Route exact path="/jardinage" component={AnnonceJardinage}/>


      </Switch>



    </Router>
  );
}

export default App;
