import React, {Component, Fragment} from 'react';
import {Formik, Form, Field} from 'formik';
import * as yup from 'yup';
import {MDBCol, MDBRow, MDBContainer, MDBIcon, MDBBtn} from "mdbreact";
import Select from 'react-select';
import '../CSS/AnimauxFormCSS.css';

class AnimauxForm extends Component{

    constructor(props){
        super(props);
        this.state ={
            animauxList: [],
            prestation: [],
            dateDebut:'',
            dateFin:'',
            debutSelection:'',
            finSelection:'',
            jourOptions:[],
            codePostal:'',
            descriptif:'',
        }
    }
    render(){
        const validationSchema = yup.object().shape({
            animauxList: yup
                        .string()
                        .required("Veuillez sélectionner au moins un animal"),
            prestation: yup
                        .string()
                        .required("Veuillez renseigner ce champ *"),
            dateDebut: yup
                        .string()
                        .required("Veuillez choisir une date de début"),
            dateFin: yup
                        .string()
                        .required("Veuillez choisir une date de fin"),
            codePostal: yup
                        .string()
                        .required("Veuillez renseigner ce champ *")
        });

        const{animaux, prestations}=this.state;
        return(
            <Fragment>
          
      <MDBContainer className="container fluid  ">
          <MDBRow>
              <h3><MDBIcon far icon="edit" /><strong>Poster une annonce </strong> </h3>
          </MDBRow>
          <br></br><br></br>
            <MDBRow className="Form">
            <MDBCol sm="10">
                    <Formik
                    initialValues={{
                        animauxList: this.state.animauxList,
                        prestation: this.state.prestation,
                        dateDebut: this.state.dateDebut,
                        dateFin: this.state.dateFin,
                        codePostal: this.state.codePostal,
                        descriptif: this.state.descriptif,
                             }}

                    onSubmit={(values, actions) => {
                        setTimeout(() => {
                          alert(JSON.stringify(values, null, 2));
                          actions.setSubmitting(false);
                        }, 1000);}}
                    
                        validationSchema={validationSchema}
                            >
                                {formikProps => (
                            <Form>
                                {/* <MDBRow>
                                <label className="label">Animal : <span style={{ color: 'red' }}>Merci de choisir au minimum un animal </span></label>
                            {formikProps.errors.animauxList && formikProps.touched.animauxList && (<p style={{ color: 'red' }}>{formikProps.errors.animauxList}</p>)}
                                </MDBRow>
                                <Select
                            className="mt-0"
                            name="animauxList"
                            placeholder="Sélectionner un ou plusieurs animaux ..."
                            value={formikProps.values.animauxList}
                            isMulti 
                            onChange={selectedOptions =>{
                                formikProps.setFieldValue('animauxList', selectedOptions)
                            }}
                            animaux={animaux.map((animal, index) =>{
                                return(
                                    {value: animal.id, label: animal.libelle}
                                    )})}
                            noOptionsMessage={() => "Aucun animal ne correspond à votre recherche"} />
                                    <br></br>
                                    <MDBRow>
                                    <label className="label">Prestation : <span style={{ color: 'red' }}>Merci de choisir au minimum une prestation </span></label>
                            {formikProps.errors.prestation && formikProps.touched.prestation && (<p style={{ color: 'red' }}>{formikProps.errors.prestation}</p>)} 
                                    </MDBRow>
                                    <Select
                            className="mt-0"
                            name="prestation"
                            placeholder="Sélectionner une prestation ..."
                            value={formikProps.values.prestation}
                            isMulti 
                            onChange={selectedOptions =>{
                                formikProps.setFieldValue('prestation', selectedOptions)
                            }}
                            prestations={prestations.map((prestation, index) =>{
                                return(
                                    {value: prestation.id, label: prestation.libelle}
                                    )})}
                            noOptionsMessage={() => "Aucun animal ne correspond à votre recherche"} /> */}
                                        <br></br>

                <MDBRow >
                  <label for="jourId"><strong>Date de début : </strong>
                   {formikProps.errors.dateDebut && formikProps.touched.dateDebut && (<small style={{ color: 'red' }}>{formikProps.errors.dateDebut}</small>)}
                  </label>
                 
              </MDBRow>
              <Field
                            name ="dateDebut"
                            id="dateDebut"
                            className="w-100"
                            type="date"
                            placeholder={this.state.dateDebut}
                            onChange={formikProps.handleChange('dateDebut')}
                        />
                        <br></br><br></br>
                <MDBRow >
                  <label for="jourId"><strong>Date de fin : </strong>
                   {formikProps.errors.dateFin && formikProps.touched.dateFin && (<small style={{ color: 'red' }}>{formikProps.errors.dateFin}</small>)}
                  </label>
                 
              </MDBRow>
              <Field
                            name ="dateFin"
                            id="dateFin"
                            className="w-100"
                            type="date"
                            placeholder={this.state.dateFin}
                            onChange={formikProps.handleChange('dateFin')}
                        />
                        <br></br><br></br>
                <MDBRow >
                            <label className="label"> <strong> Code Postal : </strong></label>
                            {formikProps.errors.codePostal && formikProps.touched.codePostal && (<span style={{ color: 'red' }}>{formikProps.errors.codePostal}</span>)}
                        </MDBRow>
                        <Field
                            name ="codePostal"
                            id="codePostal"
                            className="w-100"
                            type="text"
                            placeholder={this.state.codePostal}
                            onChange={formikProps.handleChange('codePostal')}
                        />
                        <br></br><br></br>
<div className="form-group">
           <MDBRow> <label htmlFor="exampleFormControlTextarea1">
            <strong>Descriptif de l'annonce :</strong>
            </label></MDBRow>
            <textarea
            name="descriptif"
            className="form-control"
            id="descriptif"
            rows="5"
            placeholder={this.state.descriptif}
            onChange={formikProps.handleChange('descriptif')}
            />
        </div>
                        <br></br><br></br><br></br>
<div className="btnEnvoyer justify-content-center">
<MDBBtn  color="success" type="submit" onClick={formikProps.handleSubmit}>Valider</MDBBtn>
</div>
                            </Form>
                            )}
                        </Formik>
                        </MDBCol>
    </MDBRow>
</MDBContainer>

</Fragment>
        );
    }
}

export default AnimauxForm;
