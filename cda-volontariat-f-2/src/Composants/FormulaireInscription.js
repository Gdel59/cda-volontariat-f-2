import React, { Component } from 'react';
import { MDBRow, MDBCol, MDBBtn, MDBInput, MDBContainer, MDBFormInline } from "mdbreact";
import "@fortawesome/fontawesome-free/css/all.min.css";
import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';
import '../CSS/HeaderAccueilCSS.css';
import '../CSS/FormulaireInscriptionCSS.css'
import {Formik, Form, Field} from 'formik';
import * as yup from 'yup';

class FormCreationCompte extends Component {
  constructor(props) {
    super(props);
    this.state = {
      civilite:'',
      civilites:[],
      profil:'',
      profils:[],
      nom:'',
      prenom:'',
      mail:'',
      tel:'',
      dateDeNaissance:'',
      ville:'',
      codePostal:'',
      login:'',
      mdp:'',
    };
  }

  setRadioCiv = (nr) => () => {

    this.setState({
      radioCiv: nr
    });
}

  setRadioOffre = (nr) => () => {

    this.setState({
      setRadioOffre: nr
    });
}
  onClick = nr => () => {
    this.setState({
      radio: nr
    });
  };
  render(){

    const validationSchema = yup.object().shape({
      civilite: yup
      .string()
      .label('Civilite')
      .required('Veuilez renseigner ce champ '),
      nom: yup
      .string()
      .label('Nom')
      .required('Veuilez renseigner ce champ ')
      .matches(/^[a-zA-Z-]*$/ , 'Caractères invalides'),
      prenom: yup
      .string()
      .label('Prenom')
      .required('Veuilez renseigner ce champ ')
      .matches(/^[a-zA-Z/-]*$/ , 'Caractères invalides'),
      mail: yup
      .string()
      .required('Veuilez renseigner ce champ ')
      .matches(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/ , 'Email invalide'),
      tel: yup
      .string()
      .required('Veuilez renseigner ce champ ')
      .matches(/^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/, "Format invalide"),
      dateDeNaissance: yup
      .string()
      .required('Veuilez renseigner ce champ '),
      ville:yup
      .string()
      .required('Veuilez renseigner ce champ '),
      codePostal:yup
      .string()
      .required('Veuilez renseigner ce champ ')
      .matches(/^\d{5}$/, "Format invalide"),
      login:yup
      .string()
      .required('Veuilez renseigner ce champ '),
      mdp:yup
      .string()
      .required('Veuilez renseigner ce champ '),
    });

    const {civilites, profils} = this.state;

    return <div id="Formulaire" className="">
    <MDBContainer id="containerForm">
          <MDBRow className="">
          <MDBCol size="5">
          <br></br>  
          <h2 id="membreoupas"> Pas encore membre ?</h2>
            <br></br>
Bienvenue sur Volontariat !!
Plateforme de mise en relation de personne

<h3 id="meublage"> A MEUBLER : Photo , Texte ?</h3>
          </MDBCol>
        <Formik
              initialValues={{
                civilite:this.state.civilite,
                nom:this.state.nom,
                prenom: this.state.prenom,
                mail:this.state.mail,
                tel:this.state.tel,
                dateDeNaissance:this.state.dateDeNaissance,
                ville:this.state.ville,
                codePostal:this.state.codePostal,
                login:this.state.login,
                mdp:this.state.mdp,

              }}
              validationSchema={validationSchema}>

{formikProps => (  
        <MDBCol className="jusity-content-center" size="7">
        
        <MDBRow className="d-flex justify-content-center">
        
          <MDBCol md="4" className="mb-2">
          <MDBRow>
          <label className="label">Civilité : </label>
           {formikProps.errors.civilite && formikProps.touched.civilite && (<span style={{ color: 'red' }}>{formikProps.errors.civilite}</span>)}
          </MDBRow>
          <MDBRow>
          {civilites.map((civilite, index) =>{
                                return(
                                        <MDBInput
                                            gap
                                            key = {index}                             
                                            name ="civilite"
                                            onChange={formikProps.handleChange('civilite')}
                                            onClick={this.setRadioCiv(civilite.id)} 
                                            checked={this.state.radioCiv===civilite.id ? true : false} 
                                            value={civilite.id}
                                            label={civilite.libelle}
                                            id={"radioCiv"+ civilite.id}
                                            type="radio" 
                                        />                                
                            )})}
          </MDBRow>
       
          <MDBRow className="mx-0 mt-4">
                    <label className="label">Nom : </label>
   {formikProps.errors.nom && formikProps.touched.nom && (<span style={{ color: 'red' }}>{formikProps.errors.nom}</span>)}
                        </MDBRow>
            <Field
              name="nom"
              type="text"
              id="nom"
              label="Nom"
              placeholder={this.state.nom}
              onChange={formikProps.handleChange('nom')}
              required
            />
    
          </MDBCol>
          <MDBCol md="4" className="mb-3">
          <MDBRow className="mx-0 mt-4">
                    <label className="label">Prénom : </label>
   {formikProps.errors.prenom && formikProps.touched.prenom && (<span style={{ color: 'red' }}>{formikProps.errors.prenom}</span>)}
                        </MDBRow>
            <Field
              name="prenom"
              type="text"
              id="prenom"
              label="Prenom"
              placeholder={this.state.prenom}
              onChange={formikProps.handleChange('prenom')}
              required />
  
          </MDBCol>
        </MDBRow>
       
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3">
          <MDBRow className="mx-0 mt-4">
                    <label className="label">Email : </label>
   {formikProps.errors.mail && formikProps.touched.mail && (<span style={{ color: 'red' }}>{formikProps.errors.mail}</span>)}
                        </MDBRow>
            <Field
              name="mail"
              type="email"
              id="mail"
              label="Email"
              placeholder={this.state.mail}
              onChange={formikProps.handleChange('mail')}
              required>
            </Field>
          </MDBCol >
          <MDBCol md="4" className="mb-3">
          <MDBRow className="mx-0 mt-4">
                    <label className="label">Téléphone: </label>
   {formikProps.errors.tel && formikProps.touched.tel && (<span style={{ color: 'red' }}>{formikProps.errors.tel}</span>)}
                        </MDBRow>
            <Field
              name="tel"
              type="tel"
              id="tel"
              label="Telephone"
              placeholder={this.state.tel}
              onChange={formikProps.handleChange('tel')}
              required />
          </MDBCol>
        </MDBRow>
  
        <MDBRow className="d-flex justify-content-center">
        
          <MDBCol md="8" className="mb-6">
          <MDBRow className="mx-0 mt-4">
                            <label className="label">Date de Naissance : </label>
                            {formikProps.errors.dateDeNaissance && formikProps.touched.dateDeNaissance && (<span style={{ color: 'red' }}>{formikProps.errors.dateDeNaissance}</span>)}
                        </MDBRow>
            <Field
              name="dateDeNaissance"
              type="date"
              id="adresse"
              label="Date de naissance"
              placeholder={this.state.dateDeNaissance}
              onChange={formikProps.handleChange('dateDeNaissance')}
              required>
            </Field>
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center">
        
          <MDBCol md="4" className="mb-2">
          <MDBRow className="mx-0 mt-4">
                            <label className="label">Ville : </label>
                            {formikProps.errors.ville && formikProps.touched.ville && (<span style={{ color: 'red' }}>{formikProps.errors.ville}</span>)}
                        </MDBRow>
            <Field
              name="ville"
              type="text"
              id="ville"
              label="Ville"
              placeholder={this.state.ville}
              onChange={formikProps.handleChange('ville')}
              required
            />
  
          </MDBCol>
          <MDBCol md="4" className="mb-3">
          <MDBRow className="mx-0 mt-4">
                            <label className="label">Code postal: </label>
                            {formikProps.errors.codePostal && formikProps.touched.codePostal && (<span style={{ color: 'red' }}>{formikProps.errors.codePostal}</span>)}
                        </MDBRow>
            <Field
              name="codePostal"
              type="text"
              id="codePostal"
              label="Code Postal"
              placeholder={this.state.codePostal}
              onChange={formikProps.handleChange('codePostal')}
              required />
  
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="8" className="mb-6">
            {/* <MDBInput
              name="adresse"
              type="text"
              id="adresse"
              label="Adresse"
              required>
            </MDBInput> 
             */}
             <br></br>
            <MDBCol><h3><strong>Quel est ton type d'offre ?</strong></h3></MDBCol>
              <br></br>
              {profils.map((profil, index) =>{
                                return(
              <MDBInput
                                            gap
                                            key = {index}                             
                                            name ="profil"
                                            onChange={formikProps.handleChange('profil')}
                                            onClick={this.setRadioOffre(profil.id)} 
                                            checked={this.state.setRadioProfil===profil.id ? true : false} 
                                            value={profil.id}
                                            label={profil.libelle}
                                            id={"radioProfil"+ profil.id}
                                            type="radio" 
                                        />  
                               )})}

          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center" >
          <MDBCol md="4" className="mb-3">
          <MDBRow className="mx-0 mt-4">
                            <label className="label">Login : </label>
                            {formikProps.errors.login && formikProps.touched.login && (<span style={{ color: 'red' }}>{formikProps.errors.login}</span>)}
                        </MDBRow>
            <Field
              name="login"
              type="text"
              id="login"
              label="Login"
              placeholder={this.state.login}
              onChange={formikProps.handleChange('login')}
              required
            />
        
          </MDBCol>
          <MDBCol md="4" className="mb-3">
          <MDBRow className="mx-0 mt-4">
                            <label className="label">Mot de passe : </label>
                            {formikProps.errors.mdp && formikProps.touched.mdp && (<span style={{ color: 'red' }}>{formikProps.errors.mdp}</span>)}
                        </MDBRow>
            <Field
              name="mdp"
              type="password"
              id="mdp"
              label="Mot de passe"
              placeholder={this.state.mdp}
              onChange={formikProps.handleChange('mdp')}
              required
            />
          </MDBCol>
        </MDBRow>
        
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3" ></MDBCol>
          <MDBCol md="4" className="mb-3">
            <MDBBtn color="success" type="button" >M'inscrire</MDBBtn>
          </MDBCol>
        </MDBRow>
        </MDBCol>
        )}
        </Formik>
        </MDBRow>
      </MDBContainer>
    </div>
  }}

  export default FormCreationCompte;