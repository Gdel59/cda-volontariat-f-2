import React, { Component } from 'react'
import { MDBBtn, MDBFormInline } from 'mdbreact';
import imageAnimaux from '../ImagesCartes/Animaux.jpg';
import imageCourse from '../ImagesCartes/Courses.jpg';
import imageJardin from '../ImagesCartes/Jardin.jpg';
import '../CSS/CartesCSS.css'

class CategoriesCarte extends Component{
render(){
    return(
<div className='container-fluid d-flex justify-content-center'>
    <div className='row'>
        <div className='col-md-4'>
        <div className='card text-center shadow'>
        <div className="overflow">
    <img src={imageAnimaux} className='card-img-top'/>
        </div>
        <div className='card-body text-dark'>
            <h4 className='card-title'>Animaux</h4>
            <p className='card-text text-secondary'>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
            voluptate velit esse cillum dolore eu fugiat nulla pariatur.
             Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
            </p>
            <a style={{color:'white', background:'#7C4205'}} href='/courses' className="btn btn-outline-brown">
               <strong>Poster</strong>
            </a>
            <a style={{color:'white', background:'grey'}} href='#' className="btn btn-outline-grey">
                <strong>Voir</strong>
            </a>
        </div>
        </div>
        </div>
        <div className='col-md-4'>
        <div className='card text-center shadow'>
        <div className="overflow">
    <img src={imageCourse} className='card-img-top'/>
        </div>
        <div className='card-body text-dark'>
            <h4 className='card-title'>Courses</h4>
            <p className='card-text text-secondary'>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
            voluptate velit esse cillum dolore eu fugiat nulla pariatur.
             Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
            </p>
            <a style={{color:'white', background:'#7C4205'}} href='/courses' className="btn btn-outline-brown">
               <strong>Poster</strong>
            </a>
            <a style={{color:'white', background:'grey'}} href='#' className="btn btn-outline-grey">
                <strong>Voir</strong>
            </a>
        </div>
        </div>
        </div>
        <div className='col-md-4'>
        <div className='card text-center shadow'>
        <div className="overflow">
    <img src={imageJardin} className='card-img-top'/>
        </div>
        <div className='card-body text-dark'>
            <h4 className='card-title'>Jardinage</h4>
            <p className='card-text text-secondary'>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
            voluptate velit esse cillum dolore eu fugiat nulla pariatur.
             Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
            </p>
            <a style={{color:'white', background:'#7C4205'}} href='/courses' className="btn btn-outline-brown">
               <strong>Poster</strong>
            </a>
            <a style={{color:'white', background:'grey'}} href='#' className="btn btn-outline-grey">
                <strong>Voir</strong>
            </a>
        </div>
        </div>
        </div>
    </div>
   

  </div>
    )
}
}

export default CategoriesCarte;
