import React, { useState } from 'react';
import { MDBContainer, MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBIcon, 
 MDBFormInline, MDBBtn } from 'mdbreact';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import '../CSS/HeaderAccueilCSS.css'
import "@fortawesome/fontawesome-free/css/all.min.css";
import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';

  export default function HeaderConnect() {
  
    const bgBlue = {backgroundColor: '#633402'}

    return(
      <div >
        <Router>
            <MDBContainer>
            <MDBNavbar id="header" className="NavBar" style={bgBlue} dark expand="lg" fixed="top">
              <MDBNavbarBrand href="/">
                  <strong className="white-text">Bienvenue</strong>
                  </MDBNavbarBrand>
              <MDBNavbarToggler  />
              <MDBCollapse  navbar>
                <MDBNavbarNav right>
                <MDBFormInline >
                <div>
                <MDBFormInline className="white-text">
                  
                  </MDBFormInline>
               
                </div>
                <MDBNavItem active>
                      <MDBBtn size="sm" color='red' className='white-text'
                       href="/"><MDBIcon icon="power-off"></MDBIcon></MDBBtn>
                      <br></br>
                
                  </MDBNavItem>
                  
                  </MDBFormInline>
                  
                </MDBNavbarNav>
              </MDBCollapse>
            </MDBNavbar>
            </MDBContainer>
        </Router>
      </div>
    );
  }