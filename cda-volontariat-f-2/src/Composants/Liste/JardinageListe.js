import React from 'react';
import {Card} from '../Composants/Liste/JardinageListe';


export const JardinageCardList = props => {
    return <Cardlist>
        {props.jardinageAnnonces.map(jardinageAnnonce =>
            <Card key={jardinageAnnonce.id} />
            )}
    </Cardlist>
};