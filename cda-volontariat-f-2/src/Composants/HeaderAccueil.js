import React, { useState } from 'react';
import { MDBContainer, MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBIcon, 
MDBDropdown, MDBDropdownToggle, MDBDropdownItem, MDBDropdownMenu, MDBFormInline, MDBBtn 
, MDBInput} from 'mdbreact';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import '../CSS/HeaderAccueilCSS.css'
import "@fortawesome/fontawesome-free/css/all.min.css";
import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';

  export default function HeaderAccueil() {
  
    const bgBlue = {backgroundColor: '#633402'}

    return(
      <div >
        <Router>
            <MDBContainer>
            <MDBNavbar id="header" className="NavBar" style={bgBlue} dark expand="lg" fixed="top">
              <MDBNavbarBrand href="/">
                  <strong className="white-text">MENU</strong>
                  </MDBNavbarBrand>
              <MDBNavbarToggler  />
              <MDBCollapse  navbar>
                <MDBNavbarNav right>
                <MDBFormInline >
                  <MDBFormInline className="white-text">
                    <MDBIcon icon="user"></MDBIcon>
                <MDBInput size="sm" label="Identifiant" outline className=" white-text"></MDBInput>
                </MDBFormInline>
                <div>
                <MDBFormInline className="white-text">
                  <MDBIcon icon="lock"></MDBIcon>
                  <MDBInput size="sm" label="Mot de passe" outline className="white-text" type="password"></MDBInput>
                </MDBFormInline>
                
                </div>
                <MDBNavItem active>
                      <MDBBtn size="sm" color='brown' className='white-text'
                       href="/Menu">Se connecter</MDBBtn>
                      <br></br>
                
                      <Link className="small white-text" to="/Menu">Informations de compte oubliées?</Link>
                  </MDBNavItem>
                  
                  </MDBFormInline>
                  
                </MDBNavbarNav>
              </MDBCollapse>
            </MDBNavbar>
            </MDBContainer>
        </Router>
      </div>
    );
  }