import React from 'react';

import imageJardin from '../ImagesCartes/Jardin.jpg';

const Card = (props) => {
    <div>
        <img alt="jardinage" src={imageJardin}/>
        <h1>{props.jardinageAnnonce.libelle}</h1>
    </div>
}