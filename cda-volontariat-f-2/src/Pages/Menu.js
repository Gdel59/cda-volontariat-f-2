import React, { Component } from 'react'
import HeaderConnect from '../Composants/HeaderConnect';
import CategoriesCarte from '../Composants/CartesCategories';
import '../CSS/MenuCSS.css';
export default function Menu(props) {
  

    return (
      <div>
        
        <HeaderConnect/>
        
        <div className="containerCarte">
        <CategoriesCarte/>
        </div>
        </div>
    );
  
  }